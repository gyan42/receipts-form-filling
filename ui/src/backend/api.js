import axios from "axios";

const fastapi = axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL,
  timeout: 10000,
});

const torchserve = axios.create({
  baseURL: process.env.VUE_APP_TORCH_PRED_BASE_URL,
  timeout: 10000,
});

const torchmgmnt = axios.create({
  baseURL: process.env.VUE_APP_TORCH_MGMNT_BASE_URL,
  timeout: 10000,
});

export default {
  fastapi,
  torchserve,
  torchmgmnt
}
