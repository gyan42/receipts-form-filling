# [Receipts Auto Form Filling](https://gitlab.com/gyan42/receipts-form-filling/)

A demo using Transformer model to extract information from receipts and do a form filling.

![](demo.png)


## Dataset

[Medium Blog](https://colab.research.google.com/gist/Mageswaran1989/442d575d8f5ca11b7ae12b1b061a04d3/receiptsautoformfilling.ipynb)

https://github.com/gyan42/mozhi-datasets/tree/main/sroie2019

## Model Training

[Online Colab Version](https://colab.research.google.com/gist/Mageswaran1989/442d575d8f5ca11b7ae12b1b061a04d3/receiptsautoformfilling.ipynb)

## Tesseract
- https://tesseract-ocr.github.io/tessdoc/Installation.html

## UI

```
cd ui
yarn install

yarn build 
export NODE_ENV=maclocal
yarn serve --mode $NODE_ENV
```

## API

```
export DEBUG=true
api/start.sh
uvicorn main:app --host 0.0.0.0 --port 8088 --reload
```
## Torch Serve

```
torchserve --start --model-store data/model-store --models all --ts-config configs/torch_serve_config.properties --foreground
```

## Web Links

- [AP: http://0.0.0.0:8088](http://0.0.0.0:8088)
- [Docs: http://0.0.0.0:8088/docs](http://0.0.0.0:8088/docs)
- [UI: http://localhost:8080/](http://localhost:8080/)
- [TorchModels: http://localhost:6544/models)](http://localhost:6544/models)

## Docker

https://apple.stackexchange.com/questions/373888/how-do-i-start-the-docker-daemon-on-macos

**Mac terminal setup**

```bash
docker-machine create --driver virtualbox default
docker-machine restart
eval "$(docker-machine env default)"
```

On Linux docker can bind to localhost, whereas on Mac run `docker-machine ip` to get docker, default IP is `192.168.99.100`

```bash
export DOCKER_BUILDKIT=1

# Build
docker build -t receipts-auto-form-filling-ui-mac:latest -f ops/ui/DockerfileMac .
docker build -t receipts-auto-form-filling-ui-linux:latest -f ops/ui/DockerfileLinux .
docker build -t receipts-auto-form-filling-api:latest -f ops/api/Dockerfile .


# Run
docker container run -p 8080:80 -it receipts-auto-form-filling-ui-mac:latest
docker container run -p 8080:80 -it receipts-auto-form-filling-ui-linux:latest
docker container run -p 8088:8088 -p 6543:6543 -p 6544:6544 -it receipts-auto-form-filling-api:latest


# Dockerhub, personal references, not to be followed or follow with changes
docker tag receipts-auto-form-filling-ui-mac:latest mageswaran1989/receipts-auto-form-filling-ui-mac:latest
docker push mageswaran1989/receipts-auto-form-filling-ui-mac:latest

docker tag receipts-auto-form-filling-ui-linux:latest mageswaran1989/receipts-auto-form-filling-ui-linux:latest
docker push mageswaran1989/receipts-auto-form-filling-ui-linux:latest

docker tag receipts-auto-form-filling-api:latest mageswaran1989/receipts-auto-form-filling-api:latest
docker push mageswaran1989/receipts-auto-form-filling-api:latest
```

## Docker Compose

- Mac
```
docker-compose -f ops/docker-compose-mac.yaml up
```

- Linux
```
docker-compose -f ops/docker-compose-linux.yaml up
```

Mac Links on Local machine:

- [UI: http://192.168.99.100:8080/](http://192.168.99.100:8080/)
- [API: http://192.168.99.100:8088](http://192.168.99.100:8088)
- [Docs: http://192.168.99.100:8088/docs](http://192.168.99.100:8088/docs)
- [TorchModels: http://192.168.99.100:6544/models](http://192.168.99.100:6544/models)

Linux Links on Local Machine: 
- [UI: http://localhost:8080/](http://localhost:8080/)
- [API: http://0.0.0.0:8088](http://0.0.0.0:8088)
- [Docs: http://0.0.0.0:8088/docs](http://0.0.0.0:8088/docs)
- [TorchModels: http://localhost:6544/models](http://localhost:6544/models)


## Heroku

[Reference](https://testdriven.io/blog/deploying-flask-to-heroku-with-docker-and-gitlab/)

https://dashboard.heroku.com/apps/gyan42-receipts-auto-filling/activity

**Build Image**

```bash
docker build --network=host  -t receipts-auto-filling-heroku:latest -f ops/heroku/Dockerfile .
```

**Test Run**

```bash
#with shell access
docker run -e PORT=8765 -p 8080:8765 -p 8088:5000 -p 6543:6543 -p 6544:6544 -it receipts-auto-filling-heroku:latest bin/bash

docker run -d --name receipts-auto-filling-heroku -e PORT=8765 -p 8080:8765 -p 8088:5000 -p 6543:6543 -p 6544:6544 receipts-auto-filling-heroku:latest

docker exec -it receipts-auto-filling-heroku /bin/bash

docker exec -it c7dd7ed40048 /bin/bash


```

**Push to Heroku Registry**

Note: Work in progress to fit the model with in 500MB RAM limit.

```
heroku login
heroku container:login

# Build the image
docker build -t receipts-auto-filling-heroku:latest -f ops/heroku/Dockerfile .
# Tag it for Heroku registry
docker tag receipts-auto-filling-heroku:latest registry.heroku.com/gyan42-receipts-auto-filling/web
# Push it to Heroku registry
docker push registry.heroku.com/gyan42-receipts-auto-filling/web
# Release the app for public
heroku container:release --app gyan42-receipts-auto-filling web
# Check for logs
heroku logs --app gyan42-receipts-auto-filling
```

[http://gyan42-receipts-auto-filling.herokuapp.com](http://gyan42-receipts-auto-filling.herokuapp.com)


